# Local Development Database Setup

For local development uncomment the $DATABASE variable under //DEV at the top of the routes.php file.

For live environment (godaddy), comment out the above, and uncomment the $DATABASE variable under //LIVE at the top of the routes.php file.

DB Login/Pass: comp4900 / bcitaccess