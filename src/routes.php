<?php
require 'phpmailer/PHPMailerAutoload.php';

// Routes
header('Access-Control-Allow-Origin: *');

// DEV
$DATABASE = 'mysql:host=soccer-pro-file.com;dbname=Soccer-Pro-File';

// LIVE
//$DATABASE = 'mysql:host=localhost;dbname=Soccer-Pro-File';


function getSalt() {
    $charset = 'abcdefgijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $randStringLen = 17;

    $randString = "";
    for ($i = 0; $i < $randStringLen; $i++) {
        $randString .= $charset[mt_rand(0, strlen($charset) - 1)];
    }

    return $randString;
}

function checkSessionID() {
    $sessionCheck = $dbh->prepare('SELECT * FROM User WHERE sessionID = ? AND userEmail = ?');
    $sessionCheck ->execute(array($_COOKIE['sessionID'], $params['loggedIn']));
    $session = $sessionCheck ->fetch(PDO::FETCH_ASSOC);

    if($session) {
        return 1;
    } else {
        return 0;
    }
}

function checkSessionIDApp($params) {
    $sessionCheck = $dbh->prepare('SELECT * FROM User WHERE sessionID = ? AND userEmail = ?');
    $sessionCheck ->execute(array($params['sessionID'], $params['email']));
    $session = $sessionCheck ->fetch(PDO::FETCH_ASSOC);

    if($session) {
        return 1;
    } else {
        return 0;
    }
}

$app->get('/', function ($request, $response, $args) {
    readfile('index.html');
});


$app->get('/home', function ($request, $response, $args) use ($app){
    return $response->withStatus(302)->withHeader('Location', '/index.html');
});

$app->get('/shared/{link}', function ($request, $response, $args) use ($app){
    setCookie("shareLink", $args['link'], 0, "/");
    return $response->withStatus(302)->withHeader('Location', '/shared.html');
});

$app->get('/api/hey/{name}', function ($request, $response, $args) use ($app){
    return $response->getBody()->write("hello there {$args['name']}.");
});

$app->get('/api/getChildren', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = $request->getQueryParams();
    $selectPlayer = $dbh->prepare('SELECT  playerID, firstName, lastName FROM Player WHERE parentEmail = ?');
    $selectPlayer ->execute(array($params['email']));
    $player = $selectPlayer ->fetchAll(PDO::FETCH_ASSOC);
    if($player)
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode($player));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'not found')));
});

$app->get('/api/getPlayerName', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = $request->getQueryParams();
    $selectPlayer = $dbh->prepare('SELECT  firstName, lastName FROM Player WHERE playerID = ?');
    $selectPlayer ->execute(array($params['playerID']));
    $player = $selectPlayer ->fetch(PDO::FETCH_ASSOC);
    if($player)
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode($player));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'not found')));
});


$app->get('/api/getCV', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = $request->getQueryParams();
    $selectPlayerInfo = $dbh->prepare('SELECT  firstName, lastName, nationality, dateOfBirth, coachFirstName, coachLastName, teamName, ageGroup, clubName, link, coachEmail FROM Player JOIN Team ON Player.teamID = Team.teamID JOIN AgeGroup ON Team.ageGroupID = AgeGroup.ageGroupID JOIN Club ON AgeGroup.clubID = Club.clubID WHERE playerID = ?');
    $selectPlayerInfo ->execute(array($params['playerID']));
    $playerInfo = $selectPlayerInfo ->fetchAll(PDO::FETCH_ASSOC);

    $selectPlayerCV = $dbh->prepare('SELECT  * FROM CVEvent WHERE playerID = ?');
    $selectPlayerCV ->execute(array($params['playerID']));
    $playerCV = $selectPlayerCV ->fetchAll(PDO::FETCH_ASSOC);

    $player= array_merge($playerInfo, $playerCV);

    if($player)
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode($player));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'not found')));
});

$app->get('/api/getCV/shared', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = $request->getQueryParams();

    $selectPlayerID = $dbh->prepare('SELECT playerID FROM Player WHERE link = ?');
    $selectPlayerID ->execute(array($params['link']));
    $playerID = $selectPlayerID ->fetch(PDO::FETCH_ASSOC);

    $selectPlayerInfo = $dbh->prepare('SELECT  firstName, lastName, nationality, dateOfBirth, coachFirstName, coachLastName, teamName, ageGroup, clubName, playerID FROM Player JOIN Team ON Player.teamID = Team.teamID JOIN AgeGroup ON Team.ageGroupID = AgeGroup.ageGroupID JOIN Club ON AgeGroup.clubID = Club.clubID WHERE playerID = ?');
    $selectPlayerInfo ->execute(array($playerID['playerID']));
    $playerInfo = $selectPlayerInfo ->fetch(PDO::FETCH_ASSOC);

    $selectPlayerCV = $dbh->prepare('SELECT  * FROM CVEvent WHERE playerID = ?');
    $selectPlayerCV ->execute(array($playerID['playerID']));
    $playerCV = $selectPlayerCV ->fetchAll(PDO::FETCH_ASSOC);

    $player = array_merge($playerInfo, $playerCV);

    if($player)
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode($player));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'not found')));
});

$app->get('/api/login', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = $request->getQueryParams();

    $selectUser = $dbh->prepare('SELECT userEmail, clubID, firstName, lastName, parent, coach, techDir, genManag, ageGroupCoord, sessionID FROM  User WHERE userEmail = ? AND password = ?');
    $selectUser ->execute(array($params['email'], crypt($params['password'], 'ab60e3a9302e56de5a3a384cfcb8139c')));
    $user = $selectUser ->fetch(PDO::FETCH_ASSOC);

    $sessionID = uniqid();

    setCookie("loggedIn", $user['userEmail'], 0);
    setCookie("sessionID", $sessionID, 0);

    $setSessionSQL= "UPDATE User SET seesionID = ? WHERE userEmail = ?";

    $setSession= $dbh->prepare($setSessionSQL);
    $setSession->execute(array($sessionID, $user['userEmail']));

    if($user)
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode($user));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'user not found', $user, $params, crypt($params['password'], 'ab60e3a9302e56de5a3a384cfcb8139c'))));
});

$app->get('/api/getEvaluations', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = $request->getQueryParams();

    $selectPlayerEvaluations = $dbh->prepare('SELECT * FROM  EvaluationEvent JOIN Evaluation ON EvaluationEvent.eventID = Evaluation.eventID WHERE playerID = ? AND evaluatorEmail = ?');
    $selectPlayerEvaluations ->execute(array($params['playerID'], $params['email']));
    $playerEvaluations = $selectPlayerEvaluations ->fetchAll(PDO::FETCH_BOTH);

    $selectPlayer = $dbh->prepare('SELECT  firstName, lastName FROM Player WHERE playerID = ?');
    $selectPlayer ->execute(array($params['playerID']));
    $player = $selectPlayer ->fetch(PDO::FETCH_ASSOC);

    if($playerEvaluations)
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode($playerEvaluations));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'player not found')));
});

$app->get('/api/getEvaluationRange', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = $request->getQueryParams();

    $selectPlayerEvaluations = $dbh->prepare('SELECT * FROM  EvaluationEvent JOIN Evaluation ON EvaluationEvent.eventID = Evaluation.eventID WHERE playerID = ? AND evaluatorType = ? AND eventDate BETWEEN ? AND  ?');
    $selectPlayerEvaluations ->execute(array($params['playerID'], $params['evaluatorType'], $params['dateFrom'], $params['dateTo']));
    $playerEvaluations = $selectPlayerEvaluations ->fetchAll(PDO::FETCH_BOTH);
    if($playerEvaluations)
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode($playerEvaluations));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'player not found')));
});

$app->get('/api/sendEmails', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = $request->getQueryParams();

    $allSent = 1;

    $selectEmails = $dbh->prepare('SELECT DISTINCT parentEmail, firstName, lastName, teamName, clubName FROM  Player JOIN Team ON Player.teamID = Team.teamID JOIN AgeGroup ON Team.ageGroupID = AgeGroup.ageGroupID JOIN Club ON AgeGroup.clubID = Club.clubID WHERE Club.clubID = ? AND parentEmail NOT IN (SELECT userEmail FROM User)');
    $selectEmails ->execute(array($params['clubID']));
    $emails = $selectEmails ->fetchAll(PDO::FETCH_ASSOC);

    file_put_contents('email_log.txt', "\nSend operation for club " . $params['clubID'] . ".\nOperation initiated at " . date("h:i:sa") . ".", FILE_APPEND);

    foreach($emails as $email) {
        try {
            $mail = new PHPMailer;

            $mail->setFrom('no-reply@soccer-pro-file.com', 'Soccer-Pro-File');
            $mail->addAddress($email['parentEmail']);
	    $mail->addCustomHeader("BCC: records@playerbench.net");
		
            $mail->isHTML(true);                                  // Set email format to HTML

            //$mail->Subject = 'Welcome to Soccer Pro-File';
            $mail->Subject = 'Greetings from Soccer Pro-File. Player ' . $email['firstName'] . ' ' . $email['lastName'] . ', from ' . $email['teamName'] . ', ' . $email['clubName'];
            $mail->Body    = 'To register click on this link:<br> soccer-pro-file.com/register.html?' . getSalt() . $params['clubID'] . 'h' . getSalt();
            $mail->AltBody = 'To register click on this link:<br> soccer-pro-file.com/register.html?' . getSalt() . $params['clubID'] . 'h' . getSalt();

            if(!$mail->send()) {
                $allSent = 0;
            }

            file_put_contents('email_log.txt', "'\nMessage sent OK to '" . $email['parentEmail'], FILE_APPEND);
        } catch (phpmailerException $e) {
            file_put_contents('email_log.txt', "\n" . $e->errorMessage(), FILE_APPEND);
        } catch (Exception $e) {
            file_put_contents('email_log.txt', "\n" . $e->getMessage(), FILE_APPEND);
        }
    }

    // $selectEmails = $dbh->prepare('SELECT DISTINCT coachEmail FROM  Team JOIN AgeGroup ON Team.ageGroupID = AgeGroup.ageGroupID JOIN Club ON AgeGroup.clubID = Club.clubID WHERE Club.clubID = ? AND coachEmail NOT IN (SELECT userEmail FROM User)');
    $selectEmails = $dbh->prepare('SELECT DISTINCT coachEmail, coachFirstName, coachLastName, teamName, clubName FROM  Team JOIN AgeGroup ON Team.ageGroupID = AgeGroup.ageGroupID JOIN Club ON AgeGroup.clubID = Club.clubID WHERE Club.clubID = ? AND coachEmail NOT IN (SELECT userEmail FROM User)');
    $selectEmails ->execute(array($params['clubID']));
    $emails = $selectEmails ->fetchAll(PDO::FETCH_ASSOC);

    foreach($emails as $email) {
        try{
            $mail = new PHPMailer;

            $mail->setFrom('no-reply@soccer-pro-file.com', 'Soccer-Pro-File');
            $mail->addAddress($email['coachEmail']);
            $mail->addCustomHeader("BCC: records@playerbench.net");

            $mail->isHTML(true);                                  // Set email format to HTML

            // $mail->Subject = 'Welcome to Soccer Pro-File';
            $mail->Subject = 'Greetings from Soccer Pro-File. Coach ' . $email['coachFirstName'] . ' ' . $email['coachLastName'] . ', from ' . $email['teamName'] . ', ' . $email['clubName'];
            $mail->Body    = 'To register click on this link:<br> soccer-pro-file.com/register.html?' . getSalt() . $params['clubID'] . 'h' . getSalt();
            $mail->AltBody = 'To register click on this link:<br> soccer-pro-file.com/register.html?' . getSalt() . $params['clubID'] . 'h' . getSalt();

            if(!$mail->send()) {
                $allSent = 0;
            }
            file_put_contents('email_log.txt', "'\nMessage sent OK to '" . $email['coachEmail'], FILE_APPEND);
        } catch (phpmailerException $e) {
            file_put_contents('email_log.txt', "\n" . $e->errorMessage(), FILE_APPEND);
        } catch (Exception $e) {
            file_put_contents('email_log.txt', "\n" . $e->getMessage(), FILE_APPEND);
        }
    }

    // $selectEmails = $dbh->prepare('SELECT DISTINCT coordinatorEmail FROM AgeGroup JOIN Club ON AgeGroup.clubID = Club.clubID WHERE Club.clubID = ? AND coordinatorEmail NOT IN (SELECT userEmail FROM User)');
    $selectEmails = $dbh->prepare('SELECT DISTINCT coordinatorEmail, teamName, clubName FROM AgeGroup JOIN Club ON AgeGroup.clubID = Club.clubID JOIN Team ON AgeGroup.ageGroupID = Team.ageGroupID WHERE Club.clubID = ? AND coordinatorEmail NOT IN (SELECT userEmail FROM User)');
    $selectEmails ->execute(array($params['clubID']));
    $emails = $selectEmails ->fetchAll(PDO::FETCH_ASSOC);

    foreach($emails as $email) {
        try{
            $mail = new PHPMailer;

            $mail->setFrom('no-reply@soccer-pro-file.com', 'Soccer-Pro-File');
            $mail->addAddress($email['coordinatorEmail']);
            $mail->addCustomHeader("BCC: records@playerbench.net");

            $mail->isHTML(true);

            // $mail->Subject = 'Welcome to Soccer Pro-File';
            $mail->Subject = 'Greetings from Soccer Pro-File. Coordinator ' . ', from ' . $email['teamName'] . ', ' . $email['clubName'];
            $mail->Body    = 'To register click on this link:<br> soccer-pro-file.com/register.html?' . getSalt() . $params['clubID'] . 'h' . getSalt();
            $mail->AltBody = 'To register click on this link:<br> soccer-pro-file.com/register.html?' . getSalt() . $params['clubID'] . 'h' . getSalt();

            if(!$mail->send()) {
                $allSent = 0;
            }
            file_put_contents('email_log.txt', "'\nMessage sent OK to '" . $email['coordinatorEmail'], FILE_APPEND);
        } catch (phpmailerException $e) {
            file_put_contents('email_log.txt', "\n" . $e->errorMessage(), FILE_APPEND);
        } catch (Exception $e) {
            file_put_contents('email_log.txt', "\n" . $e->getMessage(), FILE_APPEND);
        }
    }

    // $selectEmails = $dbh->prepare('SELECT DISTINCT tdEmail FROM Club WHERE Club.clubID = ? AND tdEmail NOT IN (SELECT userEmail FROM User)');
    $selectEmails = $dbh->prepare('SELECT DISTINCT tdEmail, teamName, clubName  FROM Club JOIN AgeGroup ON Club.clubID = AgeGroup.clubID JOIN Team ON AgeGroup.ageGroupID = Team.ageGroupID WHERE Club.clubID = ? AND tdEmail NOT IN (SELECT userEmail FROM User)');
    $selectEmails ->execute(array($params['clubID']));
    $emails = $selectEmails ->fetchAll(PDO::FETCH_ASSOC);

    foreach($emails as $email) {
        try{
            $mail = new PHPMailer;

            $mail->setFrom('no-reply@soccer-pro-file.com', 'Soccer-Pro-File');
            $mail->addAddress($email['tdEmail']);
            $mail->addCustomHeader("BCC: records@playerbench.net");

            $mail->isHTML(true);

            // $mail->Subject = 'Welcome to Soccer Pro-File';
            $mail->Subject = 'Greetings from Soccer Pro-File. Technical Director' . ', from ' . $email['teamName'] . ', ' . $email['clubName'];
            $mail->Body    = 'To register click on this link:<br> soccer-pro-file.com/register.html?' . getSalt() . $params['clubID'] . 'h' . getSalt();
            $mail->AltBody = 'To register click on this link:<br> soccer-pro-file.com/register.html?' . getSalt() . $params['clubID'] . 'h' . getSalt();

            if(!$mail->send()) {
                $allSent = 0;
            }
            file_put_contents('email_log.txt', "'\nMessage sent OK to '" . $email['tdEmail'], FILE_APPEND);
        } catch (phpmailerException $e) {
            file_put_contents('email_log.txt', "\n" . $e->errorMessage(), FILE_APPEND);
        } catch (Exception $e) {
            file_put_contents('email_log.txt', "\n" . $e->getMessage(), FILE_APPEND);
        }
    }

    file_put_contents('email_log.txt', "\nSend operation for club " . $params['clubID'] . ".\nOperation ended at " . date("h:i:sa") . ".", FILE_APPEND);

    if($allSent)
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode(array("message"=>"Emails sent!")));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'not all emails were sent, please try again')));
});

$app->get('/api/getCoachAgeGroups', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = $request->getQueryParams();

    $selectAgeGroups = $dbh->prepare('SELECT ageGroup FROM Team JOIN AgeGroup ON Team.ageGroupID = AgeGroup.ageGroupID WHERE coachEmail = ? ORDER BY ageGroup ASC');
    $selectAgeGroups ->execute(array($params['email']));
    $ageGroups = $selectAgeGroups ->fetchAll(PDO::FETCH_ASSOC);

    if($ageGroups)
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode($ageGroups));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'no age groups found')));
});

$app->get('/api/getCoachTeams', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = $request->getQueryParams();

    $selectTeams = $dbh->prepare('SELECT teamName, teamID, displayName FROM  Team WHERE coachEmail = ? ORDER BY teamName ASC');
    $selectTeams ->execute(array($params['email']));
    $teams= $selectTeams ->fetchAll(PDO::FETCH_ASSOC);
    $i;
    for($i = 0; $i < count($teams); $i++) {
        $selectPlayers = $dbh->prepare('SELECT playerID, firstName, lastName, number FROM Player JOIN Team  ON Player.teamID = Team.teamID WHERE Team.teamID = ? ORDER BY number ASC');
        $selectPlayers ->execute(array($teams[$i]['teamID']));
        $players= $selectPlayers ->fetchAll(PDO::FETCH_ASSOC);

        array_splice($teams[$i], 0, 0, $players);
    }

    if($teams)
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode($teams));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'no teams found')));
});

$app->get('/api/getAttributes', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = $request->getQueryParams();

    $selectAttributes = $dbh->prepare('SELECT * FROM Attribute JOIN AgeGroup ON Attribute.ageGroupID = AgeGroup.ageGroupID WHERE clubID = ? ORDER BY ageGroup ASC');
    $selectAttributes ->execute(array($params['clubID']));
    $attributes = $selectAttributes ->fetchAll(PDO::FETCH_ASSOC);

    if($attributes)
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode($attributes));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'no attributes found')));
});

$app->get('/api/getAgeGroupTeams', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = $request->getQueryParams();

    $selectTeams = $dbh->prepare('SELECT teamName, teamID, displayName FROM  Team WHERE ageGroupID= ? ORDER BY teamName ASC');
    $selectTeams ->execute(array($params['ageGroupID']));
    $teams= $selectTeams ->fetchAll(PDO::FETCH_ASSOC);

    if($teams)
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode($teams));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'no teams found')));
});

$app->get('/api/getCoordinatorTeams', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = $request->getQueryParams();

    $selectAgeGroup = $dbh->prepare('SELECT ageGroupID FROM  AgeGroup WHERE coordinatorEmail= ?');
    $selectAgeGroup ->execute(array($params['email']));
    $ageGroup = $selectAgeGroup ->fetch(PDO::FETCH_ASSOC);

    $selectTeams = $dbh->prepare('SELECT teamName, teamID, displayName FROM  Team WHERE ageGroupID= ? ORDER BY teamName ASC');
    $selectTeams ->execute(array($ageGroup['ageGroupID']));
    $teams= $selectTeams ->fetchAll(PDO::FETCH_ASSOC);
    $i;
    for($i = 0; $i < count($teams); $i++) {
        $selectPlayers = $dbh->prepare('SELECT playerID, firstName, lastName, number FROM Player JOIN Team  ON Player.teamID = Team.teamID WHERE Team.teamID = ? ORDER BY number ASC');
        $selectPlayers ->execute(array($teams[$i]['teamID']));
        $players= $selectPlayers ->fetchAll(PDO::FETCH_ASSOC);

        array_splice($teams[$i], 0, 0, $players);
    }

    if($teams)
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode($teams));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'no teams found')));
});

$app->get('/api/getTDTeams', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = $request->getQueryParams();

    $selectClub = $dbh->prepare('SELECT clubID FROM  Club WHERE tdEmail= ?');
    $selectClub ->execute(array($params['email']));
    $club= $selectClub ->fetch(PDO::FETCH_ASSOC);

    $selectAgeGroup = $dbh->prepare('SELECT ageGroupID, ageGroup FROM  AgeGroup WHERE clubID= ? ORDER BY ageGroup ASC');
    $selectAgeGroup ->execute(array($club['clubID']));
    $ageGroups = $selectAgeGroup ->fetchAll(PDO::FETCH_ASSOC);


    for($j = 0; $j < count($ageGroups); $j++) {
        $selectTeams = $dbh->prepare('SELECT teamName, teamID, displayName FROM  Team WHERE ageGroupID= ? ORDER BY teamName ASC');
        $selectTeams ->execute(array($ageGroups[$j]['ageGroupID']));
        $teams= $selectTeams ->fetchAll(PDO::FETCH_ASSOC);
        $i;
        for($i = 0; $i < count($teams); $i++) {
            $selectPlayers = $dbh->prepare('SELECT playerID, firstName, lastName, number FROM Player JOIN Team  ON Player.teamID = Team.teamID WHERE Team.teamID = ? ORDER BY number ASC');
            $selectPlayers ->execute(array($teams[$i]['teamID']));
            $players= $selectPlayers ->fetchAll(PDO::FETCH_ASSOC);
            array_splice($teams[$i], 0, 0, $players);
        }
        array_splice($ageGroups[$j], 0, 0, $teams);
    }

    if($ageGroups)
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode($ageGroups));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'no age groups found')));
});

$app->get('/api/getGMTeams', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = $request->getQueryParams();

    $selectClub = $dbh->prepare('SELECT clubID FROM  Club WHERE gmEmail= ?');
    $selectClub ->execute(array($params['email']));
    $club= $selectClub ->fetch(PDO::FETCH_ASSOC);

    $selectAgeGroup = $dbh->prepare('SELECT ageGroupID, ageGroup FROM  AgeGroup WHERE clubID= ? ORDER BY ageGroup ASC');
    $selectAgeGroup ->execute(array($club['clubID']));
    $ageGroups = $selectAgeGroup ->fetchAll(PDO::FETCH_ASSOC);


    for($j = 0; $j < count($ageGroups); $j++) {
        $selectTeams = $dbh->prepare('SELECT teamName, teamID, displayName FROM  Team WHERE ageGroupID= ? ORDER BY teamName ASC');
        $selectTeams ->execute(array($ageGroups[$j]['ageGroupID']));
        $teams= $selectTeams ->fetchAll(PDO::FETCH_ASSOC);
        $i;
        for($i = 0; $i < count($teams); $i++) {
            $selectPlayers = $dbh->prepare('SELECT playerID, firstName, lastName, number FROM Player JOIN Team  ON Player.teamID = Team.teamID WHERE Team.teamID = ? ORDER BY number ASC');
            $selectPlayers ->execute(array($teams[$i]['teamID']));
            $players= $selectPlayers ->fetchAll(PDO::FETCH_ASSOC);
            array_splice($teams[$i], 0, 0, $players);
        }
        array_splice($ageGroups[$j], 0, 0, $teams);
    }

    if($ageGroups)
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode($ageGroups));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'no age groups found')));
});


$app->post('/api/addCV', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = $request->getQueryParams();

    $addReportSQL = "INSERT INTO `Soccer-Pro-File`.`CVEvent` (`playerID`, `eventType`, `eventName`, `eventDate`, `information`, `eventCity`, `eventCountry`) VALUES (?, ?, ?, ?, ?, ?, ?);";

    $addReport = $dbh->prepare($addReportSQL);
    $addReport ->execute(array($params['playerID'], $params['eventType'], $params['eventName'], $params['date'], $params['information'], $params['eventCity'], $params['eventCountry']));

    //gets the report that was added using http://php.net/manual/en/pdo.lastinsertid.php
    $eventID = $dbh->lastInsertId();

    $getReport= $dbh->prepare('SELECT  * FROM CVEvent WHERE eventID= ?');
    $getReport->execute(array($eventID));
    $report= $getReport->fetchAll(PDO::FETCH_ASSOC);

    if($report)
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode($report));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'not created')));
});

/* ADDED BY RG
// CURL: /api/getPlayerEvaluationLast?eventID=###&playerID=###
// JSON: NOT SUPPORTED
*/
$app->get('/api/getEvaluation', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = $request->getQueryParams();
    $QUERY_getPlayerEvaluation = 
        "
        SELECT
            eventID, 
            playerID, 
            attendanceComment, 
            attitudeComment, 
            attendance, 
            attitude, 
            ability, 
            gameSense, 
            energyWorkRate, 
            technicalAbility, 
            lostPossesionResp, 
            takesInitiative, 
            firstTouch, 
            passingAbility, 
            shootingAccuracy, 
            oneOnOneDefending, 
            dribblingAbility
        FROM 
            Evaluation
        WHERE
            eventID = ?
            AND
            playerID = ? 
        ";
    $selectPlayerEvaluation = $dbh->prepare($QUERY_getPlayerEvaluation);
    $selectPlayerEvaluation->execute(array($params['eventID'], $params['playerID']));
    $PlayerEvaluation = $selectPlayerEvaluation -> fetch(PDO::FETCH_ASSOC);
    if($PlayerEvaluation)
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode($PlayerEvaluation));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'not found')));
});

$app->post('/api/addUser', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = $request->getQueryParams();

    $emailCheck= $dbh->prepare('SELECT  userEmail FROM User WHERE userEmail= ?');
    $emailCheck->execute(array($params['email']));
    $isEmail = $emailCheck->fetchAll(PDO::FETCH_ASSOC);

    if($isEmail) {
        return $response->withStatus(409)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'user already created')));
    }

    $parentCheck= $dbh->prepare('SELECT  parentEmail FROM Player WHERE parentEmail= ?');
    $parentCheck->execute(array($params['email']));
    $parent = $parentCheck->fetchAll(PDO::FETCH_ASSOC);

    if($parent) {
        $isParent = 1;
    } else {
        $isParent = 0;
    }

    $coachCheck= $dbh->prepare('SELECT  coachEmail FROM Team WHERE coachEmail = ?');
    $coachCheck->execute(array($params['email']));
    $coach= $coachCheck->fetchAll(PDO::FETCH_ASSOC);

    if($coach) {
        $isCoach= 1;
    } else {
        $isCoach= 0;
    }

    $coordCheck= $dbh->prepare('SELECT  coordinatorEmail FROM AgeGroup WHERE coordinatorEmail = ?');
    $coordCheck->execute(array($params['email']));
    $coord = $coordCheck->fetchAll(PDO::FETCH_ASSOC);

    if($coord) {
        $isCoord = 1;
    } else {
        $isCoord = 0;
    }

    $tdCheck = $dbh->prepare('SELECT  tdEmail FROM Club WHERE tdEmail= ?');
    $tdCheck->execute(array($params['email']));
    $td= $tdCheck->fetchAll(PDO::FETCH_ASSOC);

    if($td) {
        $isTD= 1;
    } else {
        $isTD= 0;
    }

    $gmCheck= $dbh->prepare('SELECT  gmEmail FROM Club WHERE gmEmail= ?');
    $gmCheck->execute(array($params['email']));
    $gm = $gmCheck->fetchAll(PDO::FETCH_ASSOC);

    if($gm) {
        $isGM= 1;
    } else {
        $isGM= 0;
    }

    if(!($isGM || $isTD || $isCoord || $isCoach || $isParent)) {
        return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'not in this club', "errorCode"=>'1')));
    }

    $addUserSQL= "INSERT INTO `User`(`clubID`, `userEmail`, `password`, `firstName`, `lastName`, `parent`, `coach`, `techDir`, `genManag`, `ageGroupCoord`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    $addUser= $dbh->prepare($addUserSQL);
    $addUser->execute(array($params['clubID'], $params['email'], crypt($params['password'], 'ab60e3a9302e56de5a3a384cfcb8139c'), $params['firstName'], $params['lastName'], $isParent, $isCoach, $isTD, $isGM, $isCoord));

    //gets the report that was added using http://php.net/manual/en/pdo.lastinsertid.php
    $userID= $dbh->lastInsertId();

    $getUser= $dbh->prepare('SELECT  * FROM User WHERE userID= ?');
    $getUser->execute(array($userID));
    $user= $getUser->fetch(PDO::FETCH_ASSOC);

    if($user)
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode($user));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'not created', "errorCode"=>'0')));
});

$app->post('/api/addGM', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = $request->getQueryParams();

    $permCheck= $dbh->prepare('SELECT  * FROM Util WHERE utility = ? AND utilID = 2');
    $permCheck->execute(array($params['activationPassword']));
    $perm = $permCheck->fetchAll(PDO::FETCH_ASSOC);

    if(!$perm) {
        return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'you do not have permission to create a club')));
    }

    $emailCheck= $dbh->prepare('SELECT  userEmail FROM User WHERE userEmail= ?');
    $emailCheck->execute(array($params['email']));
    $isEmail = $emailCheck->fetchAll(PDO::FETCH_ASSOC);

    if($isEmail) {
        return $response->withStatus(409)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'user already created')));
    }

    $addClubSQL= "INSERT INTO `Club`(`clubName`, `gmEmail`) VALUES (?, ?)";

    $addClub = $dbh->prepare($addClubSQL);
    $addClub->execute(array($params['clubName'], $params['email']));

    $clubID= $dbh->lastInsertId();

    $getClub= $dbh->prepare('SELECT  * FROM Club WHERE clubID= ?');
    $getClub->execute(array($clubID));
    $club= $getClub->fetch(PDO::FETCH_ASSOC);

    $addUserSQL= "INSERT INTO `User`(`clubID`, `userEmail`, `password`, `firstName`, `lastName`, `parent`, `coach`, `techDir`, `genManag`, `ageGroupCoord`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    $addUser= $dbh->prepare($addUserSQL);
    $addUser->execute(array($clubID, $params['email'], crypt($params['password'], 'ab60e3a9302e56de5a3a384cfcb8139c'), $params['firstName'], $params['lastName'], 0, 0, 0, 1, 0));

    //gets the report that was added using http://php.net/manual/en/pdo.lastinsertid.php
    $userID= $dbh->lastInsertId();

    $getUser= $dbh->prepare('SELECT  * FROM User WHERE userID= ?');
    $getUser->execute(array($userID));
    $user= $getUser->fetch(PDO::FETCH_ASSOC);

    if($user)
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode(array("user"=>$user, "club"=>$club)));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'not created', "errorCode"=>'0')));
});

$app->post('/api/submitEvaluation', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = $request->getQueryParams();

    $addEvaluationSQL = "INSERT INTO `Soccer-Pro-File`.`Evaluation` (`eventID`, `playerID`, `attendance`, `attendanceComment`, `attitude`, `attitudeComment`, `ability`) VALUES (?, ?, ?, ?, ?, ?, ?);";

    $addEvaluation = $dbh->prepare($addEvaluationSQL );
    $addEvaluation ->execute(array($params['eventID'], $params['playerID'], $params['attendance'], $params['attendanceComment'], $params['attitude'], $params['attitudeComment'], $params['ability']));

    $selectEvaluation = $dbh->prepare('SELECT * FROM Evaluation WHERE eventID = ? AND playerID = ?');
    $selectEvaluation->execute(array($params['eventID'], $params['playerID']));
    $evaluation = $selectEvaluation->fetch(PDO::FETCH_ASSOC);

    if($evaluation)
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode($evaluation));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'evaluation not created')));
});

$app->post('/api/addTD', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = $request->getQueryParams();

    $updateClubSQL = "UPDATE Club SET tdEmail = ? WHERE clubID = ?";

    $updateClub= $dbh->prepare($updateClubSQL );
    $updateClub->execute(array($params['email'], $params['clubID']));

    $selectClub = $dbh->prepare('SELECT * FROM Club WHERE clubID = ?');
    $selectClub ->execute(array($params['clubID']));
    $club= $selectClub ->fetch(PDO::FETCH_ASSOC);

    if($club)
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode($club));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'td not added')));
});

$app->post('/api/addAgeGroup', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = $request->getQueryParams();

    $duplicateCheck = $dbh->prepare('SELECT * FROM AgeGroup WHERE clubID = ? AND ageGroup = ? AND coordinatorEmail = ?');
    $duplicateCheck ->execute(array($params['clubID'], $params['ageGroup'], $params['email']));
    $duplicate = $duplicateCheck ->fetch(PDO::FETCH_ASSOC);

    if($duplicate) {
        return $response->withStatus(409)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'age group already created')));
    }

    $addAgeGroupSQL = "INSERT INTO `AgeGroup`(`clubID`, `ageGroup`, `coordinatorEmail`) VALUES (?, ?, ?)";

    $addAgeGroup= $dbh->prepare($addAgeGroupSQL );
    $addAgeGroup->execute(array($params['clubID'], $params['ageGroup'], $params['email']));

    $ageGroupID = $dbh->lastInsertId();

    //$selectAttributes = $dbh->prepare('SELECT utility FROM Util WHERE utilName = "defaultAttributes"');
    //$selectAttributes ->execute();
    //$attributesJSON = $selectAttributes ->fetchAll(PDO::FETCH_ASSOC);
    //$attributes = json_decode($attributesJSON['utility']);
    $attributes = array("Game Sense", "Energy/Work Rate", "Technical Ability", "Lost Possession Response", "Takes Initiative", "First Touch", "1-on-1 Defending", "Dribbling Ability", "Passing Ability", "Shooting Accuracy");

    foreach($attributes as $attribute) {
        $addAttributesSQL = "INSERT INTO `Attribute`(`ageGroupID`, `attributeName`, `active`) VALUES (?, ?, 0)";

        $addAttributes= $dbh->prepare($addAttributesSQL);
        $addAttributes->execute(array($ageGroupID, $attribute));
    }

    $selectAgeGroup = $dbh->prepare('SELECT * FROM AgeGroup WHERE ageGroupID = ?');
    $selectAgeGroup ->execute(array($ageGroupID));
    $ageGroup = $selectAgeGroup ->fetch(PDO::FETCH_ASSOC);

    if($ageGroup)
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode(array("Age Group" => $ageGroup, "Attributes JSON" => $attributesJSON, "Attributes" => $attributes)));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'age group not added')));
});

$app->post('/api/createEvaluationEvent', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = $request->getQueryParams();

    $duplicateCheck = $dbh->prepare('SELECT * FROM EvaluationEvent WHERE teamID = ? AND evaluatorEmail = ? AND evaluatorType = ? AND eventDate = ? AND eventType = ?');
    $duplicateCheck ->execute(array($params['teamID'], $params['evaluatorEmail'], $params['evaluatorType'], $params['eventDate'], $params['eventType']));
    $duplicate = $duplicateCheck ->fetch(PDO::FETCH_ASSOC);

    if($duplicate) {
        $selectEvaluation = $dbh->prepare('SELECT attendanceComment, attitudeComment, attendance, attitude, ability, gameSense, energyWorkRate, technicalAbility, lostPossesionResp, takesInitiative, firstTouch, passingAbility, shootingAccuracy, oneOnOneDefending, dribblingAbility, Player.playerID, firstName, lastName, eventID, number FROM Evaluation JOIN Player ON Evaluation.playerID = Player.playerID WHERE eventID = ? ORDER BY number ASC');
        $selectEvaluation ->execute(array($duplicate['eventID']));
        $evaluations = $selectEvaluation ->fetchAll(PDO::FETCH_ASSOC);

        return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode(array("event"=>$duplicate, "evaluations"=>$evaluations, "duplicate"=>true)));
    }

    $selectAttributes = $dbh->prepare('SELECT attributeName FROM Team JOIN AgeGroup ON Team.ageGroupID = AgeGroup.ageGroupID JOIN Attribute ON AgeGroup.ageGroupID = Attribute.ageGroupID WHERE teamID = ? AND active = 1');
    $selectAttributes ->execute(array($params['teamID']));
    $attributes = $selectAttributes ->fetchAll(PDO::FETCH_ASSOC);

    foreach($attributes as $attribute) {
        if($attribute['attributeName'] === "Game Sense") {
            $gameSense = 3;
        }

        if($attribute['attributeName'] === "Energy/Work Rate") {
            $energy = 3;
        }


        if($attribute['attributeName'] === "Technical Ability") {
            $technical = 3;
        }

        if($attribute['attributeName'] === "Lost Possession Resp") {
            $lostPossesion = 3;
        }

        if($attribute['attributeName'] === "Takes Initiative") {
            $initiative = 3;
        }

        if($attribute['attributeName'] === "First Touch") {
            $firstTouch = 3;
        }

        if($attribute['attributeName'] === "Passing Ability") {
            $passing = 3;
        }

        if($attribute['attributeName'] === "Shooting Accuracy") {
            $shooting = 3;
        }

        if($attribute['attributeName'] === "1-on-1 Defending") {
            $defending = 3;
        }

        if($attribute['attributeName'] === "Dribbling Ability") {
            $dribbling = 3;
        }
    }

    $addEventSQL = "INSERT INTO  `EvaluationEvent` (  `teamID` ,  `evaluatorEmail` ,  `evaluatorType` ,  `eventDate` ,  `eventType` ) VALUES (?, ?, ?, ?, ?)";

    $addEvent= $dbh->prepare($addEventSQL );
    $addEvent->execute(array($params['teamID'], $params['evaluatorEmail'], $params['evaluatorType'], $params['eventDate'], $params['eventType']));

    $eventID = $dbh->lastInsertId();

    $selectPlayers = $dbh->prepare('SELECT playerID FROM Player WHERE teamID = ?');
    $selectPlayers ->execute(array($params['teamID']));
    $players = $selectPlayers ->fetchAll(PDO::FETCH_ASSOC);

    $evaluations = array();
    $i = 0;

    foreach($players as $player) {
        $populateEvaluationsSQL = "INSERT INTO `Evaluation`(`eventID`, `playerID`, `attendanceComment`, `attitudeComment`, `attendance`, `attitude`, `ability`, `gameSense`, `energyWorkRate`, `technicalAbility`, `lostPossesionResp`, `takesInitiative`, `firstTouch`, `passingAbility`, `shootingAccuracy`, `oneOnOneDefending`, `dribblingAbility`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        $populateEvaluations= $dbh->prepare($populateEvaluationsSQL);
        $populateEvaluations->execute(array($eventID, $player['playerID'], "", "", 5, 3, 3, $gameSense, $energy, $technical, $lostPossesion, $initiative, $firstTouch, $passing, $shooting, $defending, $dribbling));

        $selectEvaluation = $dbh->prepare('SELECT eventID, attendanceComment, attitudeComment, attendance, attitude, ability, gameSense, energyWorkRate, technicalAbility, lostPossesionResp, takesInitiative, firstTouch, passingAbility, shootingAccuracy, oneOnOneDefending, dribblingAbility, Player.playerID, firstName, lastName, number FROM Evaluation JOIN Player ON Evaluation.playerID = Player.playerID WHERE eventID = ? AND Player.playerID = ? ORDER BY number ASC');
        $selectEvaluation ->execute(array($eventID, $player['playerID']));
        $evaluations[$i] = $selectEvaluation ->fetch(PDO::FETCH_ASSOC);
        $i++;
    }

    $selectEvent = $dbh->prepare('SELECT * FROM EvaluationEvent WHERE eventID = ?');
    $selectEvent ->execute(array($eventID));
    $event = $selectEvent ->fetch(PDO::FETCH_ASSOC);

    if($event)
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode(array("event"=>$event, "evaluations"=>$evaluations, "params"=>$params)));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'event not added')));
});

$app->post('/api/addTeam', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = $request->getQueryParams();

    $selectAgeGroup = $dbh->prepare('SELECT ageGroupID FROM AgeGroup WHERE ageGroup = ? AND clubID = ?');
    $selectAgeGroup ->execute(array($params['ageGroup'], $params['clubID']));
    $ageGroup = $selectAgeGroup ->fetch(PDO::FETCH_ASSOC);

    $addTeamSQL= "INSERT INTO `Team`(`ageGroupID`, `teamName`, `displayName`, `coachFirstName`, `coachLastName`, `coachEmail`, `assCoachEmail`, `managerEmail`) VALUES (?, ?, ?, ?, ?, ?, ?)";

    $addTeam= $dbh->prepare($addTeamSQL);
    $addTeam->execute(array($ageGroup['ageGroupID'], $params['teamName'], $params['teamName'], $params['coachFirstName'], $params['coachLastName'], $params['email'], $params['assCoachEmail'], $params['managerEmail']));

    $teamID = $dbh->lastInsertId();

    $selectTeam= $dbh->prepare('SELECT * FROM Team WHERE teamID = ?');
    $selectTeam->execute(array($teamID));
    $team = $selectTeam->fetch(PDO::FETCH_ASSOC);

    if($team)
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode($team));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'team not added')));
});

$app->post('/api/updateAgeGroup', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = $request->getQueryParams();

    $updateAgeGroupSQL = "UPDATE `AgeGroup` SET `ageGroup` = ?,`coordinatorEmail`= ? WHERE ageGroupID = ?";

    $updateAgeGroup = $dbh->prepare($updateAgeGroupSQL);
    $updateAgeGroup ->execute(array($params['ageGroup'], $params['email'], $params['ageGroupID']));

    $selectAgeGroup = $dbh->prepare('SELECT * FROM AgeGroup WHERE ageGroupID = ?');
    $selectAgeGroup ->execute(array($params['ageGroupID']));
    $ageGroup = $selectAgeGroup ->fetch(PDO::FETCH_ASSOC);

    if($ageGroup)
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode($ageGroup));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'age group not changed')));
});

$app->post('/api/updateTeam', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = $request->getQueryParams();

    $updateTeamSQL = "UPDATE `Team` SET `ageGroupID`=?,`teamName`=?,`coachFirstName`=?,`coachLastName`=?,`coachEmail`=?,`assCoachEmail`=?,`managerEmail`=? WHERE teamID = ?";

    $updateTeam = $dbh->prepare($updateTeamSQL);
    $updateTeam ->execute(array($params['ageGroupID'], $params['teamName'], $params['coachFirstName'], $params['coachLastName'], $params['email'], $params['assCoachEmail'], $params['managerEmail'], $params['teamID']));

    $selectTeam = $dbh->prepare('SELECT * FROM Team WHERE teamID = ?');
    $selectTeam ->execute(array($params['teamID']));
    $team = $selectTeam ->fetch(PDO::FETCH_ASSOC);

    if($team)
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode($team));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'age group not changed')));
});

$app->post('/api/updateTeamDisplayName', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = $request->getQueryParams();

    $updateTeamSQL = "UPDATE `Team` SET `displayName` = ? WHERE teamID = ?";

    $updateTeam = $dbh->prepare($updateTeamSQL);
    $updateTeam ->execute(array($params['displayName'], $params['teamID']));

    $selectTeam = $dbh->prepare('SELECT * FROM Team WHERE teamID = ?');
    $selectTeam ->execute(array($params['teamID']));
    $team = $selectTeam ->fetch(PDO::FETCH_ASSOC);

    if($team)
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode($team));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'display name not changed')));
});

$app->post('/api/updateEvaluation', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = json_decode($request->getBody(), true);

    if(!isset($params)) {
        $params = $request->getQueryParams();
    }

    $updateEvaluationSQL = "UPDATE Evaluation SET attendance = ?, attendanceComment = ?, attitude = ?, attitudeComment = ?, ability = ?, gameSense = ?, energyWorkRate = ?, technicalAbility = ?, lostPossesionResp = ?, takesInitiative = ?, firstTouch = ?, passingAbility = ?, shootingAccuracy = ?, oneOnOneDefending = ?, dribblingAbility = ? WHERE eventID = ? AND playerID = ?";

    $updateEvaluation = $dbh->prepare($updateEvaluationSQL);
    $updateEvaluation ->execute(array(isset($params['attendance'])? $params['attendance'] : null, isset($params['attendanceComment'])? $params['attendanceComment'] : null, isset($params['attitude'])? $params['attitude'] : null, isset($params['attitudeComment'])? $params['attitudeComment'] : null, isset($params['ability'])? $params['ability'] : null,isset($params['gameSense'])? $params['gameSense'] : null, isset($params['energyWorkRate'])? $params['energyWorkRate'] : null, isset($params['technicalAbility'])? $params['technicalAbility'] : null, isset($params['lostPossesionResp'])? $params['lostPossesionResp'] : null, isset($params['takesInitiative'])? $params['takesInitiative'] : null, isset($params['firstTouch'])? $params['firstTouch'] : null, isset($params['passingAbility'])? $params['passingAbility'] : null, isset($params['shootingAccuracy'])? $params['shootingAccuracy'] : null, isset($params['oneOnOneDefending'])? $params['oneOnOneDefending'] : null, isset($params['dribblingAbility'])? $params['dribblingAbility'] : null, $params['eventID'], $params['playerID']));

    $selectEvaluation = $dbh->prepare('SELECT * FROM Evaluation WHERE eventID = ? AND playerID = ?');
    $selectEvaluation->execute(array($params['eventID'], $params['playerID']));
    $evaluation = $selectEvaluation->fetch(PDO::FETCH_ASSOC);

    if($evaluation)
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode($evaluation));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'evaluation not updated', $params)));
});

$app->post('/api/updateAttribute', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = $request->getQueryParams();

    $updateAttributeSQL = "UPDATE Attribute SET active = ? WHERE attributeID = ?";

    $updateAttribute = $dbh->prepare($updateAttributeSQL);
    $updateAttribute ->execute(array($params['active'], $params['attributeID']));

    $selectAttribute = $dbh->prepare('SELECT * FROM Attribute WHERE attributeID = ?');
    $selectAttribute->execute(array($params['attributeID']));
    $attribute = $selectAttribute->fetch(PDO::FETCH_ASSOC);

    if($attribute)
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode($attribute));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'evaluation not created')));
});

$app->post('/api/addAttribute', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = $request->getQueryParams();

    $addAttributeSQL = "INSERT INTO `Attribute`(`ageGroupID`, `attributeName`, `active`) VALUES (?, ?, ?)";

    $addAttribute= $dbh->prepare($addAttributeSQL);
    $addAttribute->execute(array($params['ageGroupID'], $params['attributeName'], $params['active']));

    $attributeID = $dbh->lastInsertId();

    $selectAttribute = $dbh->prepare('SELECT * FROM Attribute WHERE attributeID = ?');
    $selectAttribute ->execute(array($attributeID));
    $attribute = $selectAttribute ->fetch(PDO::FETCH_ASSOC);

    if($attribute)
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode($attribute));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'attribute not added')));
});

$app->post('/api/addPlayerCSV', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = json_decode($request->getBody(), true);

    //Check if Teams have been uploaded first
    $getTeams= $dbh->prepare("SELECT teamID, teamName FROM Team JOIN AgeGroup ON Team.ageGroupID = AgeGroup.ageGroupID JOIN Club ON AgeGroup.clubID = Club.clubID WHERE Club.clubID = ?");
    $getTeams->execute(array($params['club']));
    $teams= $getTeams->fetchAll(PDO::FETCH_ASSOC);

    $csv = json_decode($params['csvFile'], true);
    $players = $csv['data'];
    $columnNames = $csv['meta']['fields'];

    if(!$csv) {
        return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'CSV file was not uploaded correctly, please try again', "params" => $params, "csv" => $params['csvFile'])));
    }
    //return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode(array("success"=>'Jesus fuck this works', "csv"=>json_decode($params['csv']))));

    if($columnNames[0] !== "Team Name") {
        return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'Column 1 does not have the right header, it should be "Team Name" not ' . $columnNames[0] . ', please try again', $columnNames)));
    }

    if($columnNames[1] !== "First Name") {
        return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'Column 2 does not have the right header, it should be "First Name", please try again')));
    }

    if($columnNames[2] !== "Last Name") {
        return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'Column 3 does not have the right header, it should be "Last Name", please try again')));
    }

    if($columnNames[3] !== "Date of Birth") {
        return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'Column 4 does not have the right header, it should be "Date of Birth", please try again')));
    }

    if($columnNames[4] !== "Nationality") {
        return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'Column 5 does not have the right header, it should be "Nationality", please try again')));
    }

    if($columnNames[5] !== "Number") {
        return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'Column 6 does not have the right header, it should be "Number", please try again')));
    }

    if($columnNames[6] !== "Emergency Contact") {
        return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'Column 7 does not have the right header, it should be "Emergency Contact", please try again')));
    }

    if($columnNames[7] !== "Parent E-mail") {
        return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'Column 8 does not have the right header, it should be "Parent E-mail", please try again')));
    }

    if(!($teams)) {
        return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'Please create teams first.')));
    } else {
        $addedPlayers = array();

        $i = 0;
        while($players[$i]['First Name']) {
            $getTeam = $dbh->prepare("SELECT teamID, teamName FROM Team JOIN AgeGroup ON Team.ageGroupID = AgeGroup.ageGroupID JOIN Club ON AgeGroup.clubID = Club.clubID WHERE Club.clubID = ? AND Team.teamName = ?");
            $getTeam ->execute(array($params['club'], $players[$i]['Team Name']));
            $team = $getTeam ->fetch(PDO::FETCH_ASSOC);

            if(!$team) {
                return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'Team: ' . $players[$i]['Team Name'] . " has not been created yet, players from row " . ($i + 2) . " onward have not been created.")));
            }

            if(!$players[$i]['First Name']) {
                return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'Player: ' . ($i + 1) . " does not have a First Name, please try again with this information.")));
            }

            if(!$players[$i]['Last Name']) {
                return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'Player: ' . ($i + 1) . " does not have a Last Name, please try again with this information.")));
            }

            $phonePattern = "/[0-9]{4}-[0-9]{2}-[0-9]{2}/";

            if(!(preg_match($phonePattern, $players[$i]['Date of Birth']) && $players[$i]['Date of Birth'])) {
                return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'Player: ' . ($i + 1) . " has an incorrect Date of Birth, the format should be YYYY-MM-DD.")));
            }

            if(!$players[$i]['Nationality']) {
                return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'Player: ' . ($i + 1) . " is missing a Nationality, please try again with this information.")));
            }

            if(!$players[$i]['Parent E-mail']) {
                return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'Player: ' . ($i + 1) . " is missing a Parent E-mail, please try again with this information.")));
            }

            $duplicateCheck = $dbh->prepare("SELECT * FROM Player WHERE `teamID` = ? AND `parentEmail` = ? AND `firstName` = ? AND `lastName` = ? AND `number` = ? AND `dateOfBirth` = ? AND `nationality` = ? AND `contactNumber` = ?");
            $duplicateCheck ->execute(array($team['teamID'], $players[$i]['Parent E-mail'], $players[$i]['First Name'], $players[$i]['Last Name'], $players[$i]['Number'], $players[$i]['Date of Birth'], $players[$i]['Nationality'], $players[$i]['Emergency Contact']));
            $duplicate = $duplicateCheck ->fetch(PDO::FETCH_ASSOC);

            if(!$duplicate) {
                $link = getSalt();

                $addPlayerSQL = "INSERT INTO `Player`(`teamID`, `parentEmail`, `firstName`, `lastName`, `number`, `dateOfBirth`, `nationality`, `contactNumber`, `link`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

                $addPlayer = $dbh->prepare($addPlayerSQL);
                $addPlayer ->execute(array($team['teamID'], $players[$i]['Parent E-mail'], $players[$i]['First Name'], $players[$i]['Last Name'], $players[$i]['Number'], $players[$i]['Date of Birth'], $players[$i]['Nationality'], $players[$i]['Emergency Contact'], $link));

                $newID = $dbh->lastInsertId();

                $getNewPlayer = $dbh->prepare("SELECT * FROM Player WHERE playerID = ?");
                $getNewPlayer ->execute(array($newID));
                $newPlayer = $getNewPlayer ->fetch(PDO::FETCH_ASSOC);
                $addedPlayers[$i] = $newPlayer;
            } else {
                $addedPlayers[$i] = $duplicate;
            }
            $i++;
        }
    }

    if(count($addedPlayers) == (count($players) - 1))
    return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode(array("success"=>'All the players from the uploaded CSV file are in our records', "CSV"=>$csv, "Params"=>$params, "$params[csvFile]"=>$params['csvFile'])));
    else
    return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'Players could not be added, please try again. If this problem persists contact a system administrator')));
});

$app->post('/api/addTeamsCSV', function ($request, $response, $args) {
    $dbh = new PDO($GLOBALS['DATABASE'], 'comp4900', 'bcitaccess', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
    $params = json_decode($request->getBody(), true);

    $csv = json_decode($params['csv'], true);
    $teams = $csv['data'];
    $columnNames = $csv['meta']['fields'];

    if(!$csv) {
        return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'CSV file was not uploaded correctly, please try again', "CSV"=>$csv, "Params"=>$params)));
    }
    //return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode(array("success"=>'Jesus fuck this works', "csv"=>json_decode($params['csv']))));

    if($columnNames[0] !== "Age Group") {
        return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'Column 1 does not have the right name, it should be Age Group not ' . $columnNames[0] . ', please try again', $columnNames)));
    }

    if($columnNames[1] !== "Team Name") {
        return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'Column 2 does not have the right name, it should be Team Name, please try again')));
    }

    if($columnNames[2] !== "Coach First Name") {
        return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'Column 3 does not have the right name, it should be Coach First Name, please try again')));
    }

    if($columnNames[3] !== "Coach Last Name") {
        return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'Column 4 does not have the right name, it should be Coach Last Name, please try again')));
    }

    if($columnNames[4] !== "Coach Email") {
        return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'Column 5 does not have the right name, it should be Coach Email, please try again')));
    }

    $addedTeams = array();

    $i = 0;
    while($teams[$i]['Team Name']) {
        if(!$teams[$i]['Age Group'] && !$teams[$i]['Team Name'] && !$teams[$i]['Coach First Name'] && !$teams[$i]['Coach Last Name'] && !$teams[$i]['Coach Email']) {
            break;
        }

        $getAgeGroup = $dbh->prepare("SELECT ageGroupID FROM AgeGroup JOIN Club ON AgeGroup.clubID = Club.clubID WHERE Club.clubID = ? AND AgeGroup.ageGroup = ?");
        $getAgeGroup ->execute(array($params['clubID'], $teams[$i]['Age Group']));
        $ageGroup = $getAgeGroup ->fetch(PDO::FETCH_ASSOC);

        if(!$ageGroup) {
            return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'Age Group: ' . $teams[$i]['Age Group'] . " has not been created yet, teams from row " . ($i + 2) . " onward have not been created.")));
        }

        if(!$teams[$i]['Team Name']) {
            return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'Team: ' . ($i + 1) . " does not have a Team Name, please try again with this information.")));
        }

        if(!$teams[$i]['Coach First Name']) {
            return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'Team: ' . ($i + 1) . " does not have a Coach First Name, please try again with this information.")));
        }

        if(!$teams[$i]['Coach Last Name']) {
            return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'Team: ' . ($i + 1) . " is missing a Coach Last Name, please try again with this information.")));
        }

        if(!$teams[$i]['Coach Email']) {
            return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'Team: ' . ($i + 1) . " is missing a Coach Email, please try again with this information.")));
        }

        $duplicateCheck = $dbh->prepare("SELECT * FROM Team WHERE `ageGroupID` = ? AND `teamName` = ? AND `coachFirstName` = ? AND `coachLastName` = ? AND `coachEmail` = ?");
        $duplicateCheck ->execute(array($ageGroup['ageGroupID'], $teams[$i]['Team Name'], $teams[$i]['Coach First Name'], $teams[$i]['Coach Last Name'], $teams[$i]['Coach Email']));
        $duplicate = $duplicateCheck ->fetch(PDO::FETCH_ASSOC);

        if(!$duplicate) {

            $addTeamSQL = "INSERT INTO `Team`(`ageGroupID`, `teamName`, `displayName`, `coachFirstName`, `coachLastName`, `coachEmail`) VALUES (?, ?, ?, ?, ?, ?)";

            $addTeam = $dbh->prepare($addTeamSQL);
            $addTeam ->execute(array($ageGroup['ageGroupID'], $teams[$i]['Team Name'], $teams[$i]['Team Name'], $teams[$i]['Coach First Name'], $teams[$i]['Coach Last Name'], $teams[$i]['Coach Email']));

            $newID = $dbh->lastInsertId();

            if(!$newID) {
                return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'Team: ' . $i + 1 . ' could not be added, please try again. If this problem persists contact a system administrator')));
            }

            $getNewTeam = $dbh->prepare("SELECT * FROM Team WHERE teamID = ?");
            $getNewTeam ->execute(array($newID));
            $newTeam = $getNewTeam ->fetch(PDO::FETCH_ASSOC);
            $addedTeams[$i] = $newTeam;
        } else {
            $addedTeams[$i] = $duplicate;
        }
        $i++;
    }

    if(count($addedTeams) == $i)
        return $response->withStatus(200)->withHeader('Content-type', 'application/json')->write(json_encode(array("success"=>'All the teams from the uploaded CSV file are in our records', "CSV"=>$csv, "Params"=>$params, "Data"=>$csv['data'])));
    else
        return $response->withStatus(404)->withHeader('Content-type', 'application/json')->write(json_encode(array("error"=>'Some teams could not be added, please try again. If this problem persists contact a system administrator', "index"=>$i)));
});